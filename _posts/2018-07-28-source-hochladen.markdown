---
layout: post
title:  "Wie kann ich meinen Quellcode bei euch hochladen?"
date:   2018-07-28 13:09:00 +0200
author: mtrnord
categories: [ Dokumentationen, Meta ]
excerpt_separator: <!--more-->
toc: true
---

Die IoT-Südschleswig Gruppe nutzt Gitlab um seinen Quellcode zu veröffentlichen. Hierzu braucht es einige Schritte um seinen Code hinzufügen zu können.
<!--more-->

# Zuerst braucht man ein Gitlab Konto
Ein Gitlab Konto aufsetzen geht ziemlich einfach. Hierzu rufst man [gitlab.com](https://gitlab.com) auf. Da finden sich eine Menge an Infos in Englisch welche eine Vielzahl der Features die uns Gitlab bietet zeigt. Doch wir wollen uns auf den `Sign In/Register` Knopf rechts oben konzentrieren und klicken den um damit auf die [Anmeldeseite](https://gitlab.com/users/sign_in) zu gelangen.

Auf dieser Seite können wir nun den `Register`-Tab anklicken. Nun kann man entweder das Formular ausfüllen, oder wenn man sein Google, Github, Twitter oder Bitbucket nutzen will eins dieser anklicken.

# "Ich habe nun ein Gitlab Konto. Wie finde und kontaktiere ich euch?"
Uns zu finden ist simpel wenn du bereits diese Seite gefunden hast, denn du kannst ganz einfach diesen Link anklicken: [https://gitlab.com/iotssl/](https://gitlab.com/iotssl/)

Um ein Mitglied werden muss ein wenig mehr gemacht werden. Hierzu solltest du zuerst zu uns in die [Matrix](https://matrix.org) kommen. Uns findest du im [#iotde:matrix.ffslfl.net Raum](https://matrix.to/#/#iotde:matrix.ffslfl.net). Zum einfachen Einstieg solltest du den Riot Client (web basiert) nutzen. Bedenke bitte hierbei, dass du ein Passwort setzen solltest, da der Name sonst weg ist.

Wenn du es geschafft hast in den Raum zu kommen, dann melde dich bitte mit deinem Projekt, welches du bei uns veröffentlichen willst in dem oben genanten Raum und erwähne, dass du dies gerne tun würdest.

Ich ([MTRNord](https://matrix.to/#/@MTRNord:matrix.ffslfl.net)) werde dich darauf hin in einer DM anschreiben. Sollte ich deine Nachricht verpassen und kein anderer mich darauf hinweisen darfst du mich gerne direkt anschreiben :)
Ich werde dich anschließend nach deinem Usernamen auf Gitlab fragen. Diesen brauche ich um dich auf Gitlab zu finden. Ich füge dich dann zu der Gruppe auf Gitlab hinzu. Du hast nun Zugriff Projekte hinzufügen.

# Ein Projekt hinzufügen
Um dein Projekt bei uns hochzuladen braucht man zuerst eine Anwendungen auf deinem Computer. Gitlab nutzt die Quellcode Verwaltung [Git](https://git-scm.com/).

## Git installieren
### Linux
Auf Linux ist die Installation super simpel. Du installierst wie bei jeder anderen Anwendung das Paket `git`. Also bei Debian oder Ubuntu müsstest du `apt-get install git` eingeben.

### Windows
Hier muss ein wenig mehr passieren. Zuerst musst du einen Installer von Git downloaden für die so genannte `Git Bash`. Diese findet man hier: [https://git-scm.com/downloads](https://git-scm.com/downloads). Auf dieser Seite musst du dann noch auf Windows klicken und der Downlaod startet.

Anschließend muss nun diese Anwendung ausgeführt werden. Diese fragt am Anfang nach Admin Rechte. Diese wird gebraucht weil es sich global auf dem System installieren muss, welches nur mit Admin Rechten möglich ist. 

Die erste Seite der Anwendung ist die Lizenz. Diese ist bei Git Bash die `GNU General Public License Version 2`. Du kannst etwas einfachere Informationen auf [https://choosealicense.com/licenses/gpl-3.0/](https://choosealicense.com/licenses/gpl-3.0/) erhalten. Aber aufpassen diese Seite ist für die Version 3. Diese haben allerdings beide die selben Ziele. Für Details sollte man die Lizenz im gesamten lesen. 

Alle folgenden Seiten können gelassen werden wie sie Standardmäßig sind. Anschließend startet das Programm Git für Windows einzurichten. 

Auf der darauf folgenden letzten Seite kann man nun den einen Haken entfernen und auf "Finish" drücken. Du hast nun erfolgreich Git installiert

### Mac OS X
*Ich bin kein Mac OS X User und übernehme hier nur die Anleitung von https://www.ralfebert.de/git/installation/*

Mac bietet 3 Optionen git zu installieren. Entweder mit einer Datei, oder Homebrew oder MacPorts.

Nutzt man den Weg der Datei (empfohlen!) kann man wie bei Windows  [https://git-scm.com/downloads](https://git-scm.com/downloads) aufrufen und anschließend da Mac auswählen. Die da gedownloadete Datei muss nun ausgeführt werden.

Die anderen beiden Optionen sind deutlich aufwändiger. Dazu bitte https://www.ralfebert.de/git/installation/ sich durchlesen in der Mac OS X Sektion


## Ein Gitlab Projekt erstellen
Um ein Gitlab Projekt zu erstellen geht man zuerst auf unsere Gitlab Gruppe: [iotssl](https://gitlab.com/iotssl/)-
Hier findest man auf der rechten Seite einen grünen Knopf, welcher die Beschriftung "New project" hat. Dieser wird einmal gedrückt und man findet sich anschließend bei einem Formular. 

In dem Formular trägt man den Namen seines Projektes unter "Project name" ein und kann anschließend auch eine Projekt Beschreibung in "Project description" eintragen.

Unter dem Bereich "Visibility Level" gibt es 3 Optionen:

1. **Privat**
	- Diese Einstellung beschreibt, dass nur man selbst das Projekt sehen könnt aber niemand aus der Gruppe oder Öffentlich, ausser man gebt manuell einer Person dazu die nötigen Rechte.
2. **Intern**
	- Intern meint, dass nur Benutzer mit Gitlab Account das Projekt sehen können. Aber niemand der nicht angemeldet ist.
3. **Öffentlich**
	- Diese Einstellung sollte selbst erklärend sein. Jeder kann das Projekt sich angucken. Wir empfehlen diese Einstellung zu nutzen.

Darauf folgend gibt es nun noch einen Haken für "Initialize repository with a README".
Diese Einstellung macht nur dann Sinn, wenn man zuvor eine Beschreibung eingegeben hat. Die Option generiert nämlich eine `README.md` Datei, welche aus dem Namen eures Projektes als Titel und darauf folgend die Beschreibung besteht.

## Den Quell Code hochladen
Um den Quell Code deines Projektes von deinem Computer hochzuladen musst du zuerst den Ordner auf deinem PC öffnen, indem du deine Projekt Dateien gespeichert hast.


Im folgenden werden wir mit dem Terminal arbeiten. Dies ist auch wieder Plattformspezifisch.

#### Linux
Hier öffnest du eine normale Konsole um Git zu verwenden.

#### Windows
Um eine Git Konsole zu öffnen kanst du mit Hilfe des rechts Klick im Ordner (nicht auf eine Datei) die Option "Git Bash here" anklicken und es öffnet sich eine Git fähige Konsole im aktuellen Ordner.

#### Mac OS X
*Bei diesem Betriebssystem habe ich keine Erfahrungen. Ich kann hier nur von anderen Anleitungen Dinge übernehmen.*

Um eine Konsole zu öffnen muss zuerst eine Einstellung einmalig getätigt werden.

In den Systemeinstellungen -> Tastatur im Bereich "Tastaturkurzbefehle" unter der Gruppe "Dienste" Bei dem Knoten "Dateien und Ordner" ein Häkchen bei "New Terminal at Folder" setzen.

Anschließend kann man im Untermenü "Dienste" die Option "New Terminal at Folder" auswählen.


### Git im Projekt einrichten

Im Folgenden nenne ich alle 3 verschiedenen Konsolen einfach "Konsole".

**Wichtig! folgende Befehle sollten in einem eigenen Unterordner ausgeführt werden! Git kann pro Projekt in einem Ordner genutzt werden. Nicht global.**

In dieser Konsole muss als erstes der Befehl `git init` ausgeführt werden. Dieser generiert einen Ordner `.git`. Dies ist das Gehirn von git und muss intakt bleiben. Unter Linux und Mac wird dieser vermutlich ausgeblendet sein.


### Eine .gitignore erstellen. Was soll alles NICHT hochgeladen werden?

Eine `.gitignore` Datei ist dafür da Dinge nicht ins Git zu schicken. Das trifft zum Beispiel auf Dateien mit passwörten und Zugangsdaten zu oder auch auf eventuelle Dateien die nur für einen selbst gedacht waren.

Eine .gitignore Datei ist sehr leicht aufgebaut. Man nimmt sich hierzu einen simplen Text Editor. **(Auf keinen Fall Open/Libe Office oder Word!!)**

Die Datei nennt man `.gitignore` und speichert die in den Obersten Ordner deines Projektes und auch niemals in Unterordner. Der Punkt am Anfang sagt, dass die Datei versteckt sein soll unter Linux.

Als Inhalt werden Datei Namen oder Ordner geschrieben. Ein Beispiel sähe so aus:

```bash
# Kommentare gehen mit Rauten am Anfang. Diese werden ignoriert

# Unter diesem Kommentar wird ein Ordner von Git ausgeschlossen. Wichtig ist hierbei der Schrägstrich am Ende, welche auch unter Windows nach rechts oben zeigen muss.
test_ordner/

# So werden Dateien ausgeschlossen:
test.ino

# Auch so genannte Wildcards gehen. Bei ordnern ginge zum Beispiel folgendes. Hierbei werden im test_ordner und alle darunter folgenden Ordner (eine Eben tief nur!)
# alle text.xml Dateien ausgeschlossen. Wichtig hierbei ist für Ordner werden 2 * genommen.
test_ordner/**/test.xml

# Für Dateien sieht das wie folgt aus. Diese werden üblicherweise genommen für alle Dateien unter einem Bestimmten Typen.
# Im folgenden zum Beispiel alle txt Dateien werden ausgeschlossen
*.txt
```

### Und wie geht das nun ins Git?
Dies macht man mit zwei weiteren Befehlen in der Konsole. Diese nennen sich `git add` und `git commit`

#### Git add
`git add` wird zum hinzufügen bzw. genauer zum Vorbereiten genutzt. Hierzu gibt man `git add .` ein. Dies bereitet alle Dateien, Ordner und Unterordner im aktuellen Ordner vor.

*Tipp: mit Hilfe von `git status` kann man genau die Änderungen sehen. Grün bedeutet vorbereitet fürs hinzufügen.*

#### Git commit
`git commit` macht das eigentliche Hinzufügen. Hierzu nutzt man es mit der Option `-m` also gibt man `git commit -m ""` ein. Zwischen die Anführungszeichen kommt ein Kommentar, welche Änderungen man gemacht hat. In unserem Szenario wäre zum Beispiel `Initiales Hochladen zu Gitlab` sinnvoll.

### Sind wir bald da???
Ja! wenn du es bis hier hin geschafft hast, dann bist du beim letzten Schritt. Dies ist das pushen. Hierzu musst du 3 Schritte ausführen. 

#### Deinen git Benutzer richtig einstellen
Git will vor der Ersten Nutzung von dir deinen Namen und deine Email wissen. Die Email und der Name werden Öffentlich hinterlegt sein.

Zuerst musst du `git config --global user.name "Marcel"` ausführen in der Konsole. Hierbei ersetzt du Marcel mit deinem Namen oder einen Usernamen der Öffentlich stehen darf.
Anschließend für die Email Adresse gibst du in die Konsole `git config --global user.email ""` ein. Hierbei ebenfalls zwischen die Anführungszeichen die Email, welche Öffentlich sein darf eintragen.

#### Git remote add
`git remote add` sagt deinem lokalem Git Programm wo dein oben erstelltes Gitlab Projekt ist. Hierfür gibst du vorbereitend in die Konsole `git remote add origin ` ein. Anschließend gehst du auf dein Gitlab Projekt und klickst unter dem Projekt Namen in der Mitte des Bildschirms "SSH" an und klickst danach auf "HTTPS". Dies macht es im folgenden leichter. Danach kopierst du die Adresse im Feld rechts neben dem Knopf und fügst diese hinter dem zuvor vorbereiteten Befehl in der Konsole ein und führst den Befehl aus.

#### Git push
`git push` ist der letzte Befehl den du brauchst. Du gibst hierzu ganz simpel `git push -u origin master` in der Konsole ein und führst den Befehl aus. Hierbei fragt er dich nach einem Benutzernamen und Passwort. Hier gibst du die Anmelde Daten von Gitlab ein. Du solltest nun den Quellcode auf Github sehen.

*Sollte hierbei eine Fehlermeldung bekommen liegt es vermutlich daran, dass bereits Dateien existieren. Dies kannst du lösen indem du `git pull origin master` ausführst. Danach kannst du `git push` nutzen.*

# Ein Projekt Updaten
Um ein Projekt upzudaten nutzen wir 3 der oben genannten Befehle.

Der erste ist `git add`. Er wird genauso wie oben benutzt. 
Der zweite ist `git commit` auch diesen verwendet man wie oben erklärt.
Nur bei dem dritten, `git push`, ist weniger zu tun. Hier muss nur noch `git push` gedrückt werden. Git merkt sich sein Ziel. Es kann jedoch sein das deine Zugangsdaten erneut erfragt werden.

Danach findest du deine Änderungen auch wieder im Gitlab.

# Weitere Fragen?
Für weitere Fragen bin ich immer offen. Hierzu erreicht man mich entweder via [@MTRNord:matrix.ffslfl.net](https://matrix.to/#/@MTRNord:matrix.ffslfl.net) privat erreichen oder in dem [#iotde:matrix.ffslfl.net Raum](https://matrix.to/#/#iotde:matrix.ffslfl.net) finden sich auch git fähige Leute.

Ansonsten gibt es noch ein kleines erweitertes Cheatsheet (leider nur Englisch) auf https://www.git-tower.com/blog/git-cheat-sheet welches eine Menge erklärt. Ich bin oben nur die grundlegenden Befehle abgegangen. Git kann noch weit mehr.