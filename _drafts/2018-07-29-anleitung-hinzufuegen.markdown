---
layout: post
title:  "Wie kann ich eine Anleitung hier veröffentlichen?"
date:   2018-07-29 15:28:00 +0200
author: mtrnord
categories: [ Dokumentationen, Meta ]
excerpt_separator: <!--more-->
toc: true
---

Die IoT-Südschleswig Gruppe nutzt Gitlab um seinen Quellcode zu veröffentlichen. Hierzu braucht es einige Schritte um seinen Code hinzufügen zu können.
<!--more-->