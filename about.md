---
layout: page
title: About
permalink: /about/
---

## Beschreibung
Dies ist die Dokumentations Seite von der IoT-Südschleswig Gruppe. Hier versuchen wir unsere Software zu dokumentieren oder zumindest auf die Dokumentation zu verlinken.

## Kontakt
Wir nutzen [Matrix](https://matrix.org) als Chat Plattform. Ihr könnt und in [#iotde:matrix.ffslfl.net](https://matrix.to/#/#iotde:matrix.ffslfl.net) erreichen.
Dieser Raum existiert im Rahmen der [IoT-Usergroup Deutschland](https://iot-usergroup.de/)